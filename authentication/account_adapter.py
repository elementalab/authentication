from allauth.account.adapter import DefaultAccountAdapter


class AccountAdapter(DefaultAccountAdapter):
    '''
    	Overrite django allauth default configuration
    '''

    def is_open_for_signup(self, request):
        '''
            Disable sign up form
    	'''
        return False
