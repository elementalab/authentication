# Autenticación con Django Allauth

Para instalar solo es necesario clonar el repositorio y dentro de la carpeta clonada ejecutar los siguiente comandos (usar de preferencia un entorno virtual y python3):

### para instalar:

```
pip install -r requirements.txt
python manage.py migrate
```

### Para correr:

```
python manage.py runserver
```

Esta aplicación web se desarrollo para esta entrada de [blog](https://elementalab.com/autenticacion-con-django-allauth/), allí se explica como instalar el entorno virtual.
